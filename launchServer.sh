#!/bin/bash
gunicorn -w 3 -b 0.0.0.0:6553 --worker-tmp-dir localtmp/ \
--log-level info \
--access-logfile localtmp/accesslog \
--error-logfile localtmp/errorlog \
spokey:app
