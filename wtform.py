from flask_wtf import FlaskForm
from wtforms import (StringField,
                     TextAreaField,
                     SubmitField,
                     PasswordField,
                     DateField,
                     SelectField, FormField, FieldList)
from wtforms.validators import (DataRequired,
                                Email,
                                EqualTo,
                                Length,
                                URL, IPAddress, Optional)

class String(FlaskForm):
    name = StringField('name', validators=[DataRequired()])

class IpAddressField(FlaskForm):
    ip = StringField('source ip address', validators=[DataRequired(), IPAddress()])

class IPAddressList(FlaskForm):
    name = FieldList(FormField(IpAddressField), min_entries=1)

class MyForm(FlaskForm):
    string = FormField(String)
    source_ip = FormField(IpAddressField)
    dest_range = FormField(IPAddressList)
    title = SelectField('Title', [DataRequired()],
                        choices=[('Farmer', 'farmer'),
                                 ('Corrupt Politician', 'politician'),
                                 ('No-nonsense City Cop', 'cop'),
                                 ('Professional Rocket League Player', 'rocket'),
                                 ('Lonely Guy At A Diner', 'lonely'),
                                 ('Pokemon Trainer', 'pokemon')])
    website = StringField('Website', validators=[URL()])
    birthday = DateField('Your Birthday', format='%m-%d-%Y', validators=[Optional()])
    submit = SubmitField('Submit')
