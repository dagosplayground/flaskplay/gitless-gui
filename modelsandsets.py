"""General set of helper functions needed for the spokey app that seem 
too small for their own class file."""

import os, sys, shutil, glob, math
import pickle
import yaml
import git
from gitstuff import TheGits
from tablewriter import CsvToHtml, yamilizer

YAML_FILE="settings.yaml"
PAGE_SIZE=100
MAX_SESSION_SIZE = 4093 # Constant in Flask 
PICKLE_NAME  = 'save.p'
GITPICKLE_NAME = 'gitty.p'
CACHE_FOLDER = "cache/" 		# include trailing slash if not empty 

class LocalModel():

	def __init__(self, readYaml=YAML_FILE): 
		"""Class for all the local data"""
		self.settingsYaml = None
		self.csvFolders = None
		self.selectedFolder = ''
		# General specializtion
		self.title = str() 
		self.firstH1 = str() 
		self.csvFileSelected = str()
		self.newBranchName = str()
		self.YAML_FILE = readYaml
	
	def readSettingsYaml(self):
		"""Reads settings yaml from disk."""
		try:
			with open(self.YAML_FILE, "r") as inyaml:
				self.settingsYaml = yaml.safe_load(inyaml)
			try: 
				self.title = self.settingsYaml['title']
			except KeyError:
				print("Key Missing: title")
			try: 
				self.firstH1 = self.settingsYaml['firstH1']
			except KeyError:
				print("Key Missing: firstH1")
		except:
			raise
	
	def getFolderNames(self):
		"""Returns a list of the folders"""
		try:
			folderList = list()
			for folder in self.settingsYaml['csvFolders']:
				folderList.append(folder['name'])

		except KeyError:
			print("Error getting the csvFolders key. Make sure readSettingsYaml was run prior, then check YAML") 
			raise

		except:
			raise
		
		self.csvFolders = folderList.copy()
		return folderList

	def saveSettingsYaml(self):
		try:
			with open(self.YAML_FILE, "w") as outyaml:
				yaml.dump(self.settingsYaml, outyaml)
		except:
			raise

class HomeModel():
	
	def __init__(self):
		self.settingsYaml = None
		self.apps = None
		self.appNames = list()
	
	def readSettingsYaml(self):
		"""Reads settings yaml from disk."""
		try:
			with open(YAML_FILE, "r") as inyaml:
				self.settingsYaml = yaml.safe_load(inyaml)
		except:
			raise
		
	def getAppNames(self):
		try: 
			appNames = list()
			for app in self.settingsYaml['apps']:
				appNames.append(app['name'])
		except KeyError:
			print("Error getting app name.")
			raise

		except:
			raise

		self.appNames = appNames.copy()
		return appNames

class DataSet():
	
	def __init__(self, cdata, sessionCopy):
		"""Pulls entire data model of csv table and sessiong information
		to be stored on the server with a pickle. Also handle data of larger 
		datasets. 
		cdata: output from GetTable from the csv to html in table writer. Update this line later
		sessionCopy: shallow copy of the session information"""
		self.csvData = cdata
		self.session = sessionCopy
		self.gitData = None # Used for storing GitLab authenticated user information
		self.PageSize = PAGE_SIZE
		self.maxPage = math.floor(len(self.csvData) / self.PageSize)
	
	def GetTable(self,page=None,pageSize=None):
		"""Get table from selected CSV stored in a pickle file 
		tablePickle: Contents of CSV 
		pageSize: Max number of rows of each page
		returns: dictionary with keys firstline and data."""
		
		retval = { "pages": 0 }
		retval['csvFirstLine'] = self.csvData['csvFirstLine'].copy()
		retval['csvData'] = self.csvData['csvData'].copy()		

		# Once table data is pulled, release page of data 
		csvlen = len(retval['csvData'])
		if page == -1: 
			pass
		else:
			csvlen = len(retval['csvData'])
			if csvlen > pageSize: # Only make changes if data needs pages
				self.maxPage = math.floor( csvlen / pageSize	)
				retval['pages'] = self.maxPage
				if page >= self.maxPage:
					retval['csvData'] = retval['csvData'][ page*pageSize : ]  # slice to end
				else:
					retval['csvData'] = retval['csvData'][page * pageSize : (page + 1)*pageSize ]# slice of 1 page
		return retval
	
	def addCsvLine(self, line):
		"""Adds line to the CSV data.
		line: row list of data
		return: void"""
		self.csvData['csvData'].append(line)
	
	def deleteCsvLine(self, index, page, pagesize):
		"""Delete row in CSV file
		index: Index on displayed page
		page: page number
		pagesize: max rows per page
		"""
		absoluteIndex = page * pagesize + index
		print(f"absoluteIndex {absoluteIndex}")
		return self.csvData['csvData'].pop(absoluteIndex)

	def replaceCsvLine(self, index, page, pagesize, lineData):
		"""Replace row in CSV file
		index: Index on displayed page
		page: page number
		pagesize: max rows per page
		lineData: row data information, should be a shallow copy 
		"""
		absoluteIndex = page * pagesize + index
		print(f"absoluteIndex {absoluteIndex}")
		self.csvData['csvData'][absoluteIndex] = lineData
	
	def getPage(index, pageSize):
		"""Get the page of the data modified.
		index: index in the list. 
		pageSize: size per page
		"""
		return index/pageSize
		
def getFolderLocation(folderName,folders):
	"""Return the location of the folder based on the name"""
	for folder in folders:
		if folder['name'] == folderName:
			return folder['location']
	raise IndexError

def getAppFile(appName, appList):
	"""Returns the settings file name for the friendly name
	appName: name to be matched with the name in the settings file. 
	appList: List of dictionaries to match.
	returns: settings file name on system"""
	for app in appList:
		if app['name'] == appName:
			return app['settingsFile']
	raise IndexError

##### </General Purpose> #####
