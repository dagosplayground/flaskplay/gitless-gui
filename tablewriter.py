import sys, os, csv, glob, math
from pathlib import Path
import yaml

class ReadmeManipulator:
	headerMarker = "# Generated Module Variables"

	def __init__(self, readmeLocation, readmeDestination=None):
		"""Reads readme file and replaces a section at the bottom with a header marker"""
		self.srcPath = readmeLocation
		self.destPath = readmeLocation if readmeDestination == None else readmeDestination
		self.makerLineNum = None
		self.docout = None
		self.foundHeader = False

	def findMarker(self): 
		"""Goes into the readme and finds the marker"""

		try:
			with open(self.srcPath,'r') as readmeFile:
				for linenum,line in enumerate(readmeFile.readlines()):
					if self.headerMarker in line:
						self.markerLineNum = linenum
						# print(f"Marker on {self.markerLineNum}")
						readmeFile.close()
		except: 
			raise

	def getReadme(self):
		"""Gets readme file and stores it the class. Reads up to the markerLineNum"""
		try:
			self.docout = str()
			with open(self.srcPath,'r+') as readmeFile:
				for line in readmeFile.readlines():
					self.docout += line 
					
					if self.headerMarker in line:
						self.foundHeader = True
						readmeFile.close()
						break
		except FileNotFoundError: 
			with open(self.srcPath,'w') as readmeFile:
				# reamefile.write('[[_TOC _]]\n')
				self.docout = "[[_TOC_]]\n" 
		except:
			raise

	def addModuleVars(self,moduleVars):
		"""Adds module variables as a string to the end of the file.
		moduleVars: String input of the module variables to add to readme."""
		if not self.foundHeader:
			try:
				self.docout += "\n" + self.headerMarker 
			except TypeError:
				print(f"Type Error found in {self.srcPath}")
				if self.docout == '':
					self.docout = "[[_TOC_]]\n"
				self.docout += "\n" + self.headerMarker 
			except:
				raise

		# self.docout += "\n"
		self.docout += moduleVars 
		self.docout += "\n"

	def writeReadme(self):
		"""Writes readme file to the destination path."""
		try: 
			Path(self.destPath).touch()
		except:
			raise

		try:
			with open(self.destPath,'w') as outreadme:
				for line in self.docout:
					outreadme.write(line)
		except:
			raise

class CsvToReadme:
	
	def __init__(self,path="."):
		"""Reads in CSV files in a folder and writes the table"""
		self.folderPath = path
		self.docout = str()
		self.csvIn = dict()

	def readCsv(self):
		"""Reads CSVs from folder defined in path. 
		returns: void"""

		for csvfile in glob.glob(self.folderPath+"/*.csv"):
			with open(csvfile,'r') as cfile:
				try:
					# self.csvIn[csvfile] = list(csv.reader(cfile, delimiter=",", quotechar='"')).copy()
					self.csvIn[csvfile] = list(csv.reader(cfile, dialect='excel')).copy()
					# spamreader = csv.reader(cfile, dialect='excel')
					# self.csvIn[csvfile] 

				except:
					print(f"Ignoring file: {cfile}")
					raise

	def ToString(self):
		"""Returns prepared contents as string"""
		return self.docout

	def GenerateTables(self):
		"""Generates tables based on file names"""

		for cfile in self.csvIn.keys():
			firstLine = True 
			for line in self.csvIn[cfile]:
				
				if not firstLine:
					self.docout += f"| { ' | '.join(line) } |\n" 
					pass
				else:
					self.docout += f"\n## Table for {cfile}\n"
					self.docout += f"| { ' | '.join(line) } |\n" 
					self.docout += f"| { ':---: | '.join(['' for x in range(len(line)+1)] ) }\n"
					firstLine = False 

class CsvToHtml:
	
	def __init__(self,path="."):
		"""Reads in CSV files in a folder and writes the table"""
		self.folderPath = path
		self.docout = str()
		self.csvIn = dict()

	def readCsv(self):
		"""Reads CSVs from folder defined in path. 
		returns: void"""

		for csvfile in glob.glob(self.folderPath+"/*.csv"):
			with open(csvfile,'r') as cfile:
				try:
					# self.csvIn[csvfile] = list(csv.reader(cfile, delimiter=",", quotechar='"')).copy()
					self.csvIn[csvfile] = list(csv.reader(cfile, dialect='excel')).copy()
					# spamreader = csv.reader(cfile, dialect='excel')
					# self.csvIn[csvfile] 

				except:
					print(f"Ignoring file: {cfile}")
					raise
	
	def writeTable(tableFile,headerList,dataList):
		"""Write table from selected CSV
		headerList: list of values to write to the CSV file
		dataList: list of lists of the rows and columns of data
		returns: void"""

		try:
			# dataToWrite = list()
			# dataToWrite.append(headerList)
			# for lineData in dataList: dataToWrite.append(lineData)

			with open(tableFile, 'w') as csvfile:
				writer = csv.writer(csvfile, dialect='excel', quoting=csv.QUOTE_NONNUMERIC)
				writer.writerow(headerList)
				writer.writerows(dataList)
		except:
			raise

	def ToString(self):
		"""Returns prepared contents as string"""
		return self.docout

	def GetTable(tableFile,page=None,pageSize=None):
		"""Get table from selected CSV
		tableFile: Contents of CSV 
		pageSize: Max number of rows of each page
		returns: dictionary with keys firstline and data."""
		retval = { "pages": 0 }
		firstLine = True
		
		try:	
			with open(tableFile, 'r') as csvfile:
				fileData  = list(csv.reader(csvfile, dialect='excel')).copy()

				for line in fileData:
					if not firstLine: 
						retval['csvData'].append([column for column in line]) 
					else:
						retval['csvFirstLine'] = [column for column in line]  # Header Data
						retval['csvData'] = list()							# Data list for later
						firstLine = False
			pass # Marker for end of 'with'
		except:
			raise

		# Once table data is pulled, release page of data 
		csvlen = len(retval['csvData'])
		if page == -1: 
			pass
		elif page != None and pageSize != None:
			csvlen = len(retval['csvData'])
			if csvlen > pageSize: # Only make changes if data needs pages
				maxpage = math.floor( csvlen / pageSize	)
				retval['pages'] = maxpage 
				if page == maxpage:
					retval['csvData'] = retval['csvData'][ page*pageSize : ]  # slice to end
				else:
					retval['csvData'] = retval['csvData'][page * pageSize : (page + 1)*pageSize ]# slice of 1 page
		elif csvlen > 130: # Just a safety from large tables 
			psize = 100
			retval['pages'] = math.floor( csvlen / psize ) # max page return
			retval['csvData'] = retval['csvData'][ : psize - 1 ]	

		return retval

	def GenerateTables(self):
		"""Generates tables based on file names"""

		for cfile in self.csvIn.keys():
			firstLine = True 
			for line in self.csvIn[cfile]:
				
				if not firstLine:
					self.docout += f"| { ' | '.join(line) } |\n" 
					pass
				else:
					self.docout += f"\n## Table for {cfile}\n"
					self.docout += f"| { ' | '.join(line) } |\n" 
					self.docout += f"| { ':---: | '.join(['' for x in range(len(line)+1)] ) }\n"
					firstLine = False 
				# if not firstLine:
				# 	self.docout += f""
				pass

class yamilizer():

	def __init__(self,yamlfile):
		"""Yaml class for reading, writing, converting, and observing. 
		yamlfile: Name of the yaml file name.
		"""
		self.yamlfile = yamlfile
		self.yamldata = None

	def readFile(self):
		"""Read Yaml file in question."""
		try:
			with open(self.yamlfile,"r") as yamin:
				self.yamldata = yaml.safe_load(yamin)
		except:
			raise

	def writeFile(self):
		"""Write yamldata to file name in class"""
		try:
			with open(self.yamlfile,"w") as yamout:
				yaml.dump(self.yamldata, yamout, sort_keys=False)
		except:
			raise

	def csvdataIn(self,headerline,csvData):
		"""Take csv data a convert information into yaml data for the class.
		headerline: list of headernames in CSV
		csvData: list of rows, each row is a list of column data. Data assumed strings
		return: void"""

		try:
			self.yamldata = list() # Ensure space is clear. 

			for row in csvData:
				tmpData = dict()
				for index,column in enumerate(row):
					tmpData[headerline[index]] = column
				self.yamldata.append(tmpData.copy())

		except: 
			raise

	def tableOut(self): 
		"""Turns the yaml data into CSV like data out.
		retrun: headerline, data. Headerline is list of strings, data row then column lists"""
		retval = dict()
		retval['csvData'] = list()
		try:
			# Take the first item for the field information to create the header fields
			retval['csvFirstLine'] = [x for x in self.yamldata[0].keys()]
			for entry in self.yamldata:
				rowList = list()
				for header in retval['csvFirstLine']:
					rowList.append(entry[header]) # Consistent order of header sequence each row
				retval['csvData'].append(rowList)
		except:
			raise

		return retval 
