import os, sys, shutil, glob, math
import pickle
import yaml
import git

from gitlabber import GitlabUser

from flask import Blueprint
from flask import render_template
from flask import abort
from flask import make_response
from flask import session
from flask import request
from flask import redirect
from flask import url_for
from flask import flash
from markupsafe import escape

from gitstuff import TheGits
from tablewriter import CsvToHtml, yamilizer
from modelsandsets import *

selfservice_app = Blueprint("selfservice_app", __name__)


##### <Self-Service App> #####
@selfservice_app.route("/selfService", methods=["GET", "POST"])
def selfService():
	try:
		lm = LocalModel(session['appSettings'])
	except KeyError:
		return redirect("/")

	try:
		appToken = session['apiToken']
		if appToken == str():
			return redirect("/login")
	except KeyError:
		return redirect("/login")

	if 'baseBranch' not in session:
		session['baseBranch'] = str()

	if 'newBranchName' not in session:
		session['newBranchName'] = str()

	lm.readSettingsYaml()
	lm.getFolderNames()
	try:
		lm.selectedFolder = session['selectedFolder']
	except KeyError:
		lm.selectedFolder = str()
		session['selectedFolder'] = str()

	if 'gitpulled' not in session:
		session['gitpulled'] = 0

	if session['showTable'] and session['gitpulled']:
		# Read in from pickle module
		ds = pickle.load(open(f"{ session['repoFolder'] }/{ PICKLE_NAME }","rb")) # Pull data set from pickle
		tmpData = ds.GetTable(session['currentPage'], ds.PageSize)
		tableData = dict()
		tableData['csvFirstLine'] = tmpData['csvFirstLine'].copy()
		tableData['csvData']      = tmpData['csvData'].copy()
		tableData['maxPage']      = tmpData['pages']
		session['maxPage']        = tmpData['pages']
		print(f"Max pages: { tmpData['pages'] }\nlen data: { len(tmpData['csvData'][:]) } ")

	else:
		#TODO: Put blank object in here when ready
		# session['showTable'] = 0
		# session['gitpullsed'] = 0
		tableData = None
		pass
	
	return render_template("self-service.html", gitdata=lm, cdata=tableData, enumerate=enumerate)

@selfservice_app.route("/setgits", methods=["POST"])
def gitBranch():
	# lm = LocalModel(session['appSettings'])
	# lm.readSettingsYaml()

	try:
		baseBranch = request.form.get("baseBranch")
		if session['baseBranch'] != baseBranch:
			session['baseBranch'] = baseBranch
		print(baseBranch)
		# if lm.settingsYaml['baseBranch'] != newBranch:
		# 	lm.settingsYaml['baseBranch'] = newBranch
		# lm.saveSettingsYaml()
	except KeyError:
		# baseBranch = request.form.get("baseBranch"):
		session['baseBranch'] = baseBranch
		raise
	except: 
		raise 
	
	return redirect("/selfService") 

@selfservice_app.route("/setBranchName", methods=["POST"])
def gitBranchName():
	# lm = LocalModel(session['appSettings'])
	# lm.readSettingsYaml()

	try:
		newBranchName = request.form.get("newBranchName")
		session['newBranchName'] = newBranchName
		# if lm.settingsYaml['newBranchName'] != newBranchName:
		# 	lm.settingsYaml['newBranchName'] = newBranchName
		# lm.saveSettingsYaml()
	except KeyError:
		newBranchName = request.form.get("newBranchName")
		session['newBranchName'] = newBranchName
		flash("New Branch Name Error")
	except: 
		raise 
	
	return redirect("/selfService") 

@selfservice_app.route("/setFolder", methods=["POST"])
def gitFolder():
	   """Used for setting the folder in the repository."""
	   try:
			   selectedFolder = request.form.get("folder") # Pull git folder from form
			   # Go through safe way to write the folder to session information
			   if "selectedFolder" in session and selectedFolder == session["selectedFolder"]:
					   # print(f"ping folder select { session['selectedFolder'] }")
					   pass
			   elif selectedFolder != '' or selectedFolder != 'selectedFolder':
					   session["selectedFolder"] = selectedFolder  # Set here to ensure values stays graphically
					   # print(f"Selected folder: { selectedFolder }")
	   except:
			   print("Failed to get git folder from form")
			   flash("Failed to get git folder from form")
			   raise

	   return redirect("/selfService")

@selfservice_app.route("/gitpull", methods=["POST"])
def gitPull():
	lm = LocalModel(session['appSettings'])
	lm.readSettingsYaml()
	lm.getFolderNames()
  
	if "selectedFolder" in session and '' != session["selectedFolder"]:
		print("Selected folder found.")
		try:
			glu = GitlabUser(session['apiToken'])
			session['repoFolder'] = f"{CACHE_FOLDER}{glu.username}"
			# print("before second try")
			# print(f"{CACHE_FOLDER}{glu.username}")
			# print(f"RepoFolder: { session['repoFolder'] }")
			try:
				print(f"current repo folder {session['repoFolder']}")
				if session['repoFolder'] == str():
					print("Empty repo folder")
					return redirect("/selfService")
			except KeyError:
				print("Key error report")
				return redirect("/selfService")
				
			lm.selectFolder = glu.username 
			
			# Checkout git repository from GitLab with Api Access Token. gr = git repo
			try:
				gr = TheGits(session['repoFolder'], f"https://{glu.username}:{session['apiToken']}@{lm.settingsYaml['repo']}")
				gr.getRepo(glu.username, glu.email)
				gr.createBranch(session['baseBranch'], session['newBranchName'])
			except git.exc.GitCommandError:
				try:
					gr = pickle.load(open(f"{ session['repoFolder'] }/{ GITPICKLE_NAME }","rb")) # Pull git repo object from pickle
					flash("Pulling git information from pickle")
				except:
					raise
			except:
				raise

			# Set flag for git information
			session['gitpulled'] = 1
			session['showTable'] = 0
			# Create a list of CSV's to select
			try:	
				location = getFolderLocation(session['selectedFolder'],lm.settingsYaml['csvFolders'])
			except IndexError:
				print("Select folder is incorrect.")
				return redirect("/selfService")

			session['csvFiles'] = list()
			print(f"Test location: { session['repoFolder'] }{ location }/*.csv")
			for csvfile in glob.glob(f"{ session['repoFolder'] }{ location }/*.csv"):
				print(csvfile)
				session['csvFiles'].append(csvfile)
			for csvfile in glob.glob(f"{ session['repoFolder'] }{ location }/*ml"):
				print(csvfile)
				session['csvFiles'].append(csvfile)
			session['csvFileSelcted'] = str() 

			# Save git repository object information.
			pickle.dump(gr, open(f"{ session['repoFolder'] }/{ GITPICKLE_NAME }","wb"))

		except:
			print("Exception handled gitpull")
			raise
	else: 
		print("Failure: No folder selected")

	print("function finished")
	return redirect("/selfService") 
  
@selfservice_app.route("/gitdel", methods=["POST"])
def gitDel():
	lm = LocalModel(session['appSettings'])
	lm.readSettingsYaml()
  
	session['gitpulled'] = 0
	if "selectedFolder" in session and '' != session["selectedFolder"]:
		print("valid")
		try:
			# session['gitpulled'] = 0
			session['csvFileSelected'] = '' 
			session['csvFirstLine'] = None
			session['csvData'] = None
			session['showTable'] = 0 
			shutil.rmtree(session['repoFolder'])
			session['currentPage'] = 0
			session['maxPage'] = 0
		except FileNotFoundError:
			print(f"Folder { session['repoFolder']} already deleted") 
		except:
			raise
	else: 
		print("Failure: No folder selected")

	return redirect("/selfService") 

@selfservice_app.route("/gitcommit", methods=["POST"])
def gitCommit():
	"""Git Commit command"""
	# Check if the folder exists and there is git information
	# try:
	# 	gr = pickle.load(open(f"{ session['repoFolder'] }/{ GITPICKLE_NAME }","rb")) # Pull git repo object from pickle
	# except:
	# 	raise
	
	return render_template("commit.html")

@selfservice_app.route("/csvSelect", methods=["POST"])
def selectCsv():
	lm = LocalModel(session['appSettings'])
	lm.readSettingsYaml()
	
	pickedCsv = request.form.get("csvFile")	

	if pickedCsv != "" or pickedCsv != None:
		try:
			session['csvFileSelected'] = pickedCsv
			# print(f"Picked CSV: {pickedCsv}")

			cdata = None

			# Check file extension for type of data
			if pickedCsv[-3:] == "csv":
				cdata = CsvToHtml.GetTable(pickedCsv,-1,PAGE_SIZE) 
			elif pickedCsv[-3:] == "yml" or pickedCsv[-4:] == "yaml":
				yml = yamilizer(pickedCsv)
				yml.readFile()	
				cdata = yml.tableOut().copy()

			# session['csvFirstLine'] = cdata['csvFirstLine'].copy()
			session['showTable'] = 1
			session['currentPage'] = 0 
			
			# Setup and pickle 
			ds = DataSet(cdata.copy(), session.copy())
			
			# Set the table data from DataSet class
			rightsize = False
			tmpPageSize = PAGE_SIZE
			ds.PageSize = tmpPageSize
			session['maxPage'] = ds.maxPage # Set how many pages
			pickle.dump( ds, open(f"{ session['repoFolder'] }/{ PICKLE_NAME }","wb"))

		except TypeError:
			print("No CSV was selected.")
			flash("No CSV was selected.")
			session['showTable'] = 0
		except FileNotFoundError:
			flash("File was found, select a folder and git pull again.")
		except: 
			raise
	try:
		print(f"Max pages: { session['maxPage'] }")
	except KeyError:
		print("No max page can be found.")
		flash("No max page can be found.")
	return redirect("/selfService")

@selfservice_app.route("/tablePage", methods=["POST"])
def setTablePage():
	"""Selects which page to display from the CSV"""
	pageNumber = int(request.form.get("currentPage"))
	print(f"page number: { pageNumber }")
	if pageNumber > session['maxPage']:
		pageNumber = session['maxPage']
	try:
		# cdata = CsvToHtml.GetTable(session['csvFileSelected'],pageNumber,PAGE_SIZE) 
		ds = pickle.load(open(f"{ session['repoFolder'] }/{ PICKLE_NAME }","rb")) # Pull data set from pickle
		rightsize = False 		# Used to make sure table can be displayed. 
		while not rightsize:
			cdata = ds.GetTable(pageNumber, ds.PageSize)
			# session['csvFirstLine'] = cdata['csvFirstLine'].copy()
			# session['csvData'] = cdata['csvData'].copy()
			# session['maxPage'] = cdata['pages'] 
			session['currentPage'] = pageNumber
			if sys.getsizeof(session) > MAX_SESSION_SIZE:
				ds.PageSize -= 1
			else:
				rightsize = True
	
		# Save current state to the pickle 
		ds.session = session.copy()	
		pickle.dump( ds, open(f"{ session['repoFolder'] }/{ PICKLE_NAME }","wb"))
		
	except:
		raise	

	return redirect("/selfService")

@selfservice_app.route("/cancelCsvTable", methods=["POST"])
def cancelCsvTable():
	try:
		# gr = pickle.load(open(f"{ session['repoFolder'] }/{ GITPICKLE_NAME }","rb")) # Pull git repo object from pickle

		print("load pickle")
		cdata = None
		# Check file extension for type of data
		if session['csvFileSelected'][-3:] == "csv":
			cdata = CsvToHtml.GetTable(session['csvFileSelected'],-1,PAGE_SIZE) 
		elif session['csvFileSelected'][-3:] == "yml" or session['csvFileSelected'][-4:] == "yaml":
			yml = yamilizer(session['csvFileSelected'])
			yml.readFile()	
			cdata = yml.tableOut().copy()

		ds = DataSet(cdata.copy(), session.copy())
		print("Save Pickle")

		pickle.dump(ds, open(f"{ session['repoFolder'] }/{ PICKLE_NAME }","wb"))
		session['showTable'] = 1
	except:
		raise
	
	return redirect("/selfService")

@selfservice_app.route("/writeCsvTable", methods=["POST"])
def writeCsvTable():
	try:
		ds = pickle.load(open(f"{ session['repoFolder'] }/{ PICKLE_NAME }","rb")) # Pull data set from pickle
		gr = pickle.load(open(f"{ session['repoFolder'] }/{ GITPICKLE_NAME }","rb")) # Pull git repo object from pickle
		if session['csvFileSelected'][-3:] == 'csv':
			CsvToHtml.writeTable(session['csvFileSelected'], \
				ds.csvData['csvFirstLine'], \
				ds.csvData['csvData']) 
		elif session['csvFileSelected'][-3:] == 'yml' or session['csvFileSelected'][-4:] == 'yaml':
			yml = yamilizer(session['csvFileSelected'])
			yml.csvdataIn(ds.csvData['csvFirstLine'], ds.csvData['csvData']) 
			yml.writeFile()
		# After writing file, add file to future commit. 
		gr.addFile(session['csvFileSelected'][len(session['repoFolder']) + 1:]) # remove leading folder and slash
		pickle.dump(gr, open(f"{ session['repoFolder'] }/{ GITPICKLE_NAME }","wb"))
	except:
		raise
	
	return redirect("/selfService")

@selfservice_app.route("/deleteLine", methods=["POST"])
def deleteLineCsv():
	ds = pickle.load(open(f"{ session['repoFolder'] }/{ PICKLE_NAME }","rb")) # Pull data set from pickle
	delLine = int(request.form.get("tableIndex"))
	ds.deleteCsvLine(delLine, session['currentPage'], ds.PageSize)
	pickle.dump(ds, open(f"{ session['repoFolder'] }/{ PICKLE_NAME }","wb"))

	return redirect("/selfService")

@selfservice_app.route("/addmodify", methods=["POST"])
def addModifyForm():
	"""Adds to the form or modifies a form."""
	ds = pickle.load(open(f"{ session['repoFolder'] }/{ PICKLE_NAME }","rb")) # Pull data set from pickle
	if "Add" == session['addormodify'] or "Copy" == session['addormodify']:
		print("Add button from form pressed") 
		addLine = list()
		for element in ds.csvData['csvFirstLine']:
			tmpIn = request.form.get("new" + element)
			print(f"{element} : {tmpIn}")
			addLine.append(tmpIn)
		ds.addCsvLine(addLine)
	elif "Modify" == session['addormodify']:
		print("Modify button from form pressed") 
		modifiedLine = list()
		for element in ds.csvData['csvFirstLine']:
			tmpIn = request.form.get("new" + element)
			print(f"{element} : {tmpIn}")
			modifiedLine.append(tmpIn)
		ds.replaceCsvLine(session['modLine'], session['currentPage'], PAGE_SIZE, modifiedLine.copy())
		
	# session['csvData'] = session['csvData'].copy() # This line will be a performance issue with very large tables 
	# session['csvData'] = ds.GetTable(session['currentPage'],PAGE_SIZE)['csvData'].copy()

	pickle.dump(ds, open(f"{ session['repoFolder'] }/{ PICKLE_NAME }","wb"))
	csvLineData = None
	return redirect("/selfService")
##### </Self-Service App> #####
