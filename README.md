[[_TOC_]]

# Config GUI 

Basic web GUI to interact with git and GitLab to edit config files in yaml or csv in a web spreadsheet interface. 

# Installation
1. Set up GitLab to have access via API key authentication. 
1. Launch virtualenv in this folder after checkout. 
1. To install dependencies `pip install -r requirements`
1. Run webserver `python spokey 6553` or `python3 spokey 6553`
1. Open a browser to `http://localhost:6553` to see the webpage. 

That's it for now. 

