import git
import json
import time
import glob
import os

from pathlib import Path

class TheGits():

	def __init__(self, mpath, repolink):
		"""Git of Terraform location"""
		self.repolink = repolink 
		self.modulePath = mpath
		self.repo = None
		self.branchName = f'CsvUpdate-{ time.strftime("%Y-%m-%d-%H%M") }' 
		self.branchTime = time.strftime("%Y-%m-%d-%H%M")

	def getRepo(self, username, useremail):
		"""Clones repository from source. User data used at commit time.
		username: Username associated with the api token
		useremail: User email associated with the api token
		returns: None"""
		try:
			self.repo = git.Repo.clone_from(self.repolink, self.modulePath)
		except git.exc.GitCommandError as e:
			print(f"Error with repo: {self.repolink}\nOr module path:{self.modulePath}")
			raise
		try:
			with open(f"{self.modulePath}/.git/config", "a") as repoConfig:
				user = "\n[user]\n"
				user += f"\tname = {username}\n" 
				user += f"\temail = {useremail}\n" 
				repoConfig.write(user)
		except:
			raise

	def createBranch(self, fromBranch='develop', toBranch=None):
		"""Creates a branch name to update documentation for variables table and switches to it."""
		if toBranch == None:
			toBranch = self.branchName	
		if self.repo is not None:
			self.repo.git.checkout(fromBranch)
			newbranch = self.repo.create_head(toBranch, fromBranch)
			self.repo.git.checkout(toBranch)
		else:
			print(f"{ self.repo }")
			raise TypeError
	
	def addFile(self, filePath):
		"""Adds a file to the repository.
		filePath: relative path to the file to add to a commit.
		return: void"""
		try:
			self.repo.git.add(filePath)
		except:
			raise
	
	def commit(self, commitMessage):
		"""Commit message to repo.
		commitMessage: String of a commit message.
		return: void"""
		try:
			self.repo.git.commit(f"-m \"{commitMessage}\"")
		except:
			raise

	def pushRepo(self, branchName):
		"""Pushes branch to master."""
		try:
			self.repo.git.push("-u","origin", f"{branchName}")	
		except: 
			self.repo.git.push("origin", f"{branchName}")
			print("Branch already exists.")

	def pushOrigin(self):
		"""Push a fresh branch and commit to origin for review."""
		self.repo.git.push("-u","origin", f"{ self.branchName }")
		print(f"Pushed repo: {self.branchName} ")
