"""
What's going on here? Well what was supposed to be a simple gui is now turning into a webapp that
only can be run locally because hosting is expensive.
"""
import os, sys, shutil, glob, math
import pickle
import yaml
import git

from gitlabber import GitlabUser

from flask import Flask
from flask import render_template
from flask import make_response
from flask import session
from flask import request
from flask import redirect
from flask import url_for
from flask import flash
from markupsafe import escape

# Blueprints to load for each page in app
from dashboard import dashboard_app
from login import login_app
from commitgit import commitgit_app
from selfservice import selfservice_app

from gitstuff import TheGits
from tablewriter import CsvToHtml, yamilizer
from wtform import MyForm
from modelsandsets import *

##### </General Purpose> #####

# YAML_FILE="settings.yaml"
# PAGE_SIZE=100
# MAX_SESSION_SIZE = 4093 # Constant in Flask 
# PICKLE_NAME  = 'save.p'
# GITPICKLE_NAME = 'gitty.p'
# CACHE_FOLDER = "cache/" 		# include trailing slash if not empty 
app = Flask(__name__)

#### <App registration> ####
app.register_blueprint(dashboard_app)
app.register_blueprint(login_app)
app.register_blueprint(commitgit_app)
app.register_blueprint(selfservice_app)
#### </App registration> ####

# app.secret_key = os.urandom(16)
app.secret_key = b'\x14\xef\n{\xbc\x85q\x051\xf9\xb5\xfe\x12\xb9\x04\x95' # If hosting this permanently, hide key


##### <HOME App> #####

@app.route("/", methods=["GET", "POST"])
def home():
	hm = HomeModel()
	hm.readSettingsYaml()
	hm.getAppNames()

	if request.form:	
		try:			
			session['showTable'] = 0
			session['appSelect'] = request.form.get("appSelect")
			session['appSettings'] = getAppFile(session['appSelect'], hm.settingsYaml['apps'])
		except:
			raise

	try: 
		if session['appSelect'] == None: # test that key in dictionary exists
			pass
	except KeyError:
		session['appSelect'] = str()
	
	# flash("This is a message")
	# flash("This is another message")
		
	return render_template("home.html", appData=hm) 


##### <\HOME App> #####


##### <\AddModify Form> #####
@app.route("/modifyLine", methods=["POST"])
def modifyLineCsv():
	try:
		ds = pickle.load(open(f"{ session['repoFolder'] }/{ PICKLE_NAME }","rb")) # Pull data set from pickle
		modLine = int(request.form.get("tableIndex"))

		session['modLine'] = modLine
		ds.csvData['csvLineData'] = dict()
		session['addormodify'] = 'Modify'
		for num,element in enumerate(ds.csvData['csvFirstLine']):
			ds.csvData['csvLineData'][element] = dict()
			ds.csvData['csvLineData'][element]['oldEntry'] = ds.csvData['csvData'][modLine][num]
			ds.csvData['csvLineData'][element]['newEntry'] = ds.csvData['csvData'][modLine][num]
		print(f"Modify line nutmber: {modLine}")
	except:
		raise

	return render_template("addModTable.html", cdata=ds.csvData.copy(), enumerate=enumerate) 

@app.route("/copyLine", methods=["POST"])
def copyLineCsv():
	try:
		ds = pickle.load(open(f"{ session['repoFolder'] }/{ PICKLE_NAME }","rb")) # Pull data set from pickle
		modLine = int(request.form.get("tableIndex"))

		session['modLine'] = modLine
		ds.csvData['csvLineData'] = dict()
		session['addormodify'] = 'Copy'
		for num,element in enumerate(ds.csvData['csvFirstLine']):
			ds.csvData['csvLineData'][element] = dict()
			ds.csvData['csvLineData'][element]['oldEntry'] = ds.csvData['csvData'][ds.PageSize*session['currentPage'] + modLine][num]
			ds.csvData['csvLineData'][element]['newEntry'] = ds.csvData['csvData'][ds.PageSize*session['currentPage'] + modLine][num]
		print(f"Copy line number: {modLine}")

	except:
		raise

	return render_template("addModTable.html", cdata=ds.csvData.copy(), enumerate=enumerate) 

@app.route("/addLine", methods=["POST"])
def addLineCsv():
	"""Adds a line to the end of the data set."""
	try:
		# session['csvLineData'] = [ str() ] * len(session['csvFirstLine'])
		ds = pickle.load(open(f"{ session['repoFolder'] }/{ PICKLE_NAME }","rb")) # Pull data set from pickle
		ds.csvData['csvLineData'] = dict()
		session['addormodify'] = 'Add'
		for element in ds.csvData['csvFirstLine']:
			ds.csvData['csvLineData'][element] = dict()
			ds.csvData['csvLineData'][element]['oldEntry'] = str()
			ds.csvData['csvLineData'][element]['newEntry'] = str()

		# print(f"first line len = { len(ds.csvData['csvFirstLine']) }")
		# print(f"first line len = { len(ds.csvData['csvLineData']) }")

	except:
		raise
	
	return render_template("addModTable.html", cdata=ds.csvData.copy(),  enumerate=enumerate) 

##### <\AddModify Form> #####


#### <Health Check App> ####
@app.route("/healthcheck", methods=["GET"])
def healthCheck():
	status = "ok"
	return f"Health Check: {status}\n"
#### <\Health Check App> ####

#### <Place Holder Apps> #####

@app.route("/form", methods=["GET", "POST"])
def form():
	form = MyForm()
	if form.validate_on_submit():
		return redirect("/selfService") 
	return render_template("form.html", form=form)

@app.route("/admin", methods=["GET", "POST"])
def admin():
		
	return render_template("dashboard.html", len=len)

#### <\Place Holder Apps> #####

if __name__ == '__main__':
#	readSettingsYaml()
	app.run(host='0.0.0.0', port=sys.argv[1])
