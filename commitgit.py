from flask import Blueprint
from flask import render_template
from flask import abort
from flask import session
from flask import flash
from flask import request
from flask import redirect

from modelsandsets import *

commitgit_app = Blueprint("commitgit_app", __name__)

#### <Git Commit App> ####

@commitgit_app.route("/commitgit", methods=["POST"])
def commitGit():
	"""Commits and pushes the git commit message."""
	lm = LocalModel(session['appSettings'])
	lm.readSettingsYaml()
	try:
		cm = request.form.get("commitMessage")
		gr = pickle.load(open(f"{ session['repoFolder'] }/{ GITPICKLE_NAME }","rb")) # Pull git repo object from pickle
		gr.commit(cm)
		gr.pushRepo(session["newBranchName"])
	except:
		print("Couldn't find the git repo pickle")
		flash("Couldn't find the git repo pickle")
		raise
	
	if "selectedFolder" in session and '' != session["selectedFolder"]:
		print("valid")
		try:
			session['gitpulled'] = 0
			session['csvFileSelected'] = '' 
			session['csvFirstLine'] = None
			session['csvData'] = None
			session['showTable'] = 0 
			shutil.rmtree(session['repoFolder'])
			session['currentPage'] = 0
			session['maxPage'] = 0
		except FileNotFoundError:
			print(f"Folder { session['repoFolder']} already deleted") 
			flash(f"Folder { session['repoFolder']} already deleted") 
		except:
			raise

	return redirect("/selfService")

#### <\Git Commit App> ####
