import gitlab

GITLAB_URL = 'https://gitlab.com/'

class GitlabUser():
	
	def __init__(self,accessToken):
		"""Validates and accessed the gilablab user information from the API
		accessToken: User personal access token
		"""
		self.accessToken = accessToken
		self.auth = False

		self.gl = gitlab.Gitlab(GITLAB_URL,private_token=self.accessToken)

		self.errorMsg = str()

		self.authenticate()
		try:
			self.username = self.gl.user.username
			self.name = self.gl.user.name 
			self.email = self.gl.user.email
		except AttributeError:
			self.username = str()
			self.name = str()
			self.email = str()
		except:
			raise
	
	def authenticate(self):
		"""Authenticates user"""
		try:
			if not self.auth:
				self.gl.auth()
				self.auth = True
		except gitlab.exceptions.GitlabAuthenticationError as e:
			# TODO: Do something here to show a problem
			self.errorMsg = e.error_message
			self.auth = False
		except: 
			raise

	def GetProjectID(self, projectName):
		"""Gets project ID or raises error if not found with user credentials"""
		retval = str()
		try:
			retval = self.gl.projects.list(search=projectName)[0].id
			
		except gitlab.exceptions.GitlabAuthenticationError:
			print(f"Cannot find the project id for {project}")
		except:
			raise
		return retval
	
	def getProjectName(self, projectid):
		"""Gets project ID and find the proeject name.
		projecid: project id number
		returns: Name string"""
		try:
			projectName = self.gl.projects.get(projectid).name
		except:
			raise
	
		return projectName

	def getUserMergeRequests(self, group):
		"""Gets all opened merge requests. Will only return merge requests made by user.
		Adds glProjectName field for the project name. Everything else from GitLab Merge API.
		group: group or group/subgroup in GitLab."""
		try:
			projectNames = dict()
			groupMrs = self.gl.groups.get(group)
			retval = list()
			for mr in groupMrs.mergerequests.list(state='opened', order_by='updated_at'):
				if mr.state == 'opened' and mr.author['username'] in self.username:
				# if mr.state != 'merged':
				# if mr.auther['username'] == self.gl.user.username:
					projectName = str()
					try:
						projectName = projectNames[mr.project_id]
					except KeyError:
						projectName = self.getProjectName(mr.project_id)
						projectNames[mr.project_id] = projectName # Save for reuse later
					mr.glProjectName = projectName
					retval.append(mr)
		except:
			raise

		return retval
	
	

