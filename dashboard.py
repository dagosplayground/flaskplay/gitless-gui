"""Dashboard app for the Spokey appliaction"""

from flask import Blueprint
from flask import render_template
from flask import abort
from flask import session
from flask import make_response
from flask import request
from flask import redirect
from flask import url_for
from flask import flash
from markupsafe import escape

from gitlabber import GitlabUser 

dashboard_app = Blueprint("dashboard_app", __name__)

PROJECT_GROUP = 'dagosplayground'

@dashboard_app.route("/dashboard", methods=["GET", "POST"])
def dashboard():
	try:
		try:
			# TODO: Add a flash message to login why redirect
			appToken = session['apiToken']
			if appToken == str():
				return redirect("/login")
		except KeyError:
			return redirect("/login")	
		
		glu = GitlabUser(session['apiToken'])
		mrs = list()
		for mr in glu.getUserMergeRequests(PROJECT_GROUP):
			mrs.append({
				"projectName" : mr.glProjectName,
				"author" : mr.author['username'],
				"sourceBranch" : mr.source_branch,
				"targetBranch" : mr.target_branch,
				"mrTitle" : mr.title,
				"mrUrl" : mr.web_url
			})
			print(f"{mrs[-1]}\n")

		print(f"Merge Request Length: {len(mrs)}")
	except:
		print("Error in dashboard")
		raise
	return render_template("dashboard.html",open_merges=mrs,user=glu.name,len=len)
