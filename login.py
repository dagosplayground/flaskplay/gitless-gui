from flask import Blueprint
from flask import render_template
from flask import abort
from flask import session
from flask import make_response
from flask import request
from flask import redirect
from flask import url_for
from flask import flash
from markupsafe import escape

from gitlabber import GitlabUser 

login_app = Blueprint("login_app", __name__)


##### <Login App> #####
@login_app.route("/login", methods=["GET", "POST"])
def login():
	# Make sure api token is handled first thing if blank
	if 'apiToken' not in session:
		session['apiToken'] = str()
	
	# Set up login model
	loginModel = dict()
	loginModel['error'] = False 
	loginModel['user'] = str()

	# Check if error, then set user if valid 
	if session['apiToken'] is not None and session['apiToken'] != str():
		glu = GitlabUser(session['apiToken'])
		if glu.auth == False:
			session['apiToken'] = str()
			loginModel['error'] = glu.errorMsg
		else:
			loginModel['user'] = glu.name

	return render_template("login.html", lm=loginModel)

@login_app.route("/setAccessToken", methods=["POST"])
def loginSetApiToken():
	"""Just grab the Api token and put in session information"""
	session["apiToken"] = request.form.get("newApiToken")

	return redirect("/login")

@login_app.route("/clearAccessToken", methods=["POST"])
def loginClearApiToken():
	"""Just grab the Api token and put in session information"""
	session["apiToken"] = str() 
	return redirect("/login")

##### <\Login App> #####
